package com.myshopify.ecomOptimus.Locators;

public class Locators {

	public class OnBoardingLocators {
		public static final String INPUT_EMAIL = "PasswordNewsletterForm-email";
		public static final String LINK_PASSWORD_LOGIN = "//header[@class='password-header']//a[1]";
		public static final String INPUT_PASSWROD = "Password";
		public static final String BUTTON_SUBMIT = "(//button[@type='submit'])[last()]";
		public static final String SPAN_INVALID_PASSWORD = "PasswordLoginForm-password-error";

	}

	public class BasePageLocator {
		public static final String HEADER_LOGO = "site-header__logo-link";
		public static final String LINK_FOOTER = "Powered by Shopify";
		public static final String BUTTON_SEARCH = "//button[contains(@class,'site-header__search')]";
		public static final String INPUT_SEARCH = "//input[@placeholder='Search']";
		public static final String SEARCH_RESULTS = "//li[starts-with(@id,'search-result')]";
		public static final String LINK_CART_COUNT = "CartCount";

	}

	public class HomeLocator {
		public static final String PAGE_VALIDATOR = "shopify-section-hero-1";
		public static final String LIST_FEATURED_COLLECTION = "//div[@id='shopify-section-collection']//li";

	}

	public class AddToCartLoctor {
		public static final String BUTTON_ADD_TO_CART = "//button[@name='add']";
		public static final String SELECT_COLOR = "SingleOptionSelector-0";
		public static final String SELECT_SIZE = "SingleOptionSelector-1";
		public static final String COLOR_SIZE_DEATILS = "//li[contains(@class,'product-details__item')]";
		public static final String BUTTON_CONTINUE_SHOPPING = "//button[contains(@class,'cart-popup__dismiss-button')]";
		public static final String CART_POPUP = "cart-popup";
	}

	public class CartLoctor {
		public static final String PAGE_VALIDATOR = "cart-header__title";
		public static final String TABLE_COLUMN = "//tbody/tr[1]//td";
		public static final String TABLE_ROW = "//tbody/tr";
		public static final String SUB_TOTAL = "//span[@class='cart-subtotal__price']";
	}

}
