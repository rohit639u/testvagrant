package com.myshopify.ecomOptimus.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.myshopify.ecomOptimus.Locators.Locators;
import com.myshopify.ecomOptimus.utils.RandomDataUtils;
import com.myshopify.ecomOptimus.utils.WaitUtils;
import com.myshopify.ecomOptimus.utils.WebdriverUtils;

import io.qameta.allure.Step;

public class HomePage extends BasePage {

	@FindBy(id = Locators.HomeLocator.PAGE_VALIDATOR)
	private WebElement pageValidator;

	@FindBy(xpath = Locators.HomeLocator.LIST_FEATURED_COLLECTION)
	private List<WebElement> featuredCollection;

	private int itemToClick;

	public HomePage() {
		Assert.assertTrue(pageValidator(), "Failed as Home page validation could not be compelted.");
		// System.out.println("hello I am list"+searchResults.size());
	}

	@Override
	public boolean pageValidator() {
		return WaitUtils.isElemntVisble(pageValidator);
	}

	@Step("Verify user able to click on search and verify size of search.")
	public boolean searchSize(String searchItem, int size) {
		try {
			clickSearch();
			ItemToSearch(searchItem);
			Assert.assertEquals(searchResults.size(), size,
					"Failed as the search result item size is not as expected as : " + size + " but, its as "
							+ searchResults.size());
			return true;
		} catch (Exception e) {
			logger.error("Failed due to exception caused by : ", e);
			return false;
		}
	}

	@Step("Select item from featured products.")
	public AddToCartPage selectFeaturedCollection() {
		Assert.assertTrue(WaitUtils.isElemntVisble(featuredCollection),
				"Failed as all featured collection is not visiable.");
		this.itemToClick = RandomDataUtils.randomNumber(0, featuredCollection.size() - 1);
		WebElement featuredItem = featuredCollection.get(this.itemToClick);
		WebdriverUtils.scrolIntoElement(featuredItem);
		Assert.assertTrue(WaitUtils.isElemntclickable(featuredItem),
				"Failed to click on random featured collection Item");
		featuredItem.click();
		return new AddToCartPage();
	}

	@Step("Verify user able to click on search.")
	public AddToCartPage clickonSearchItem(String searchItem) {

		clickSearch();
		ItemToSearch(searchItem);
		Assert.assertTrue(WaitUtils.isElemntVisble(searchResults), "Failed as all search results is not visiable.");
		this.itemToClick = RandomDataUtils.randomNumber(0, searchResults.size() - 1);
		WebdriverUtils.getActionMethod().moveToElement(searchResults.get(this.itemToClick))
				.click(searchResults.get(itemToClick)).build().perform();
		logger.info("Successfully clicked on searched Item.");
		return new AddToCartPage();
	}

}
