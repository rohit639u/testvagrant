package com.myshopify.ecomOptimus.pages;

import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.myshopify.ecomOptimus.Locators.Locators;
import com.myshopify.ecomOptimus.utils.BaseWebdriver;
import com.myshopify.ecomOptimus.utils.LoggerClass;
import com.myshopify.ecomOptimus.utils.WaitUtils;
import com.myshopify.ecomOptimus.utils.WebdriverUtils;

import io.qameta.allure.Step;

public class BasePage implements PageInterface {

	protected static final Logger logger = LoggerClass.createLogger();

	@FindBy(className = Locators.BasePageLocator.HEADER_LOGO)
	protected WebElement logoHeader;

	@FindBy(linkText = Locators.BasePageLocator.LINK_FOOTER)
	protected WebElement footerLink;

	@FindBy(xpath = Locators.BasePageLocator.BUTTON_SEARCH)
	protected WebElement searchButton;

	@FindBy(xpath = Locators.BasePageLocator.INPUT_SEARCH)
	protected WebElement searchBox;

	@FindBy(xpath = Locators.BasePageLocator.SEARCH_RESULTS)
	protected List<WebElement> searchResults;

	@FindBy(id = Locators.BasePageLocator.LINK_CART_COUNT)
	protected WebElement cartCountLink;

	protected static final String footerText = "2020, ecom.optimus Powered by Shopify";

	public BasePage() {
		PageFactory.initElements(BaseWebdriver.getDriver(), this);
		Assert.assertTrue(headerAndFooterValidation(), "Failed to verify header and footer are same.");
	}

	protected boolean headerAndFooterValidation() {
		try {
			Assert.assertTrue(WaitUtils.isElemntVisble(logoHeader),
					"Failed as logo of the organistaion is not present.");
			Assert.assertTrue(WaitUtils.isElemntVisble(footerLink),
					"Failed as link \"Powered by Shopify\" not present in footer");
			return true;
		} catch (Exception e) {
			logger.error("Failed to verify the header and footer of the page");
			return false;
		}
	}

	protected void clickSearch() {
		Assert.assertTrue(WaitUtils.isElemntclickable(searchButton),
				"Failed as serach button is not clickable on home page.");
		searchButton.click();
		logger.info("Successfully clicked on search button.");
	}

	@Step("Verify user able to search products")
	protected void ItemToSearch(String searchItem) {
		Assert.assertTrue(WaitUtils.isElemntVisble(searchBox), "Failed as search button is not visiable.");
		searchBox.clear();
		searchBox.sendKeys(searchItem);
		logger.info("Successfully send keys into search box as, : " + searchItem);
	}

	public CartPage navigateToCartPage() {
		Assert.assertTrue(WaitUtils.isElemntclickable(cartCountLink), "Failed as cart link is not clickable");
		WebdriverUtils.scrolIntoElement(cartCountLink);
		cartCountLink.click();
		return new CartPage();
	}

	@Override
	public boolean pageValidator() {
		// TODO Auto-generated method stub
		return false;
	}

}
