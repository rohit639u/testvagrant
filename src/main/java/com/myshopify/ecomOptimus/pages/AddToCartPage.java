package com.myshopify.ecomOptimus.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.myshopify.ecomOptimus.Locators.Locators;
import com.myshopify.ecomOptimus.utils.RandomDataUtils;
import com.myshopify.ecomOptimus.utils.SelectUtils;
import com.myshopify.ecomOptimus.utils.WaitUtils;

import io.qameta.allure.Step;

public class AddToCartPage extends BasePage {

	@FindBy(xpath = Locators.AddToCartLoctor.BUTTON_ADD_TO_CART)
	private WebElement addToCartButton;

	@FindBy(xpath = Locators.AddToCartLoctor.BUTTON_CONTINUE_SHOPPING)
	private WebElement continueShoppingButton;

	@FindBy(id = Locators.AddToCartLoctor.SELECT_COLOR)
	private WebElement colorSelect;

	@FindBy(id = Locators.AddToCartLoctor.SELECT_SIZE)
	private WebElement sizeSelect;

	@FindBy(xpath = Locators.AddToCartLoctor.COLOR_SIZE_DEATILS)
	private List<WebElement> colorSizeDeatil;

	@FindBy(className = Locators.AddToCartLoctor.CART_POPUP)
	private WebElement cartPop;

	private String beforeAddColor;
	private String beforeAddSize;
	private String AfterAddColor;
	private String AfterAddSize;

	public AddToCartPage() {
		Assert.assertTrue(pageValidator(), "Failed as \"add to cart\" link is not clickable");
	}

	@Override
	public boolean pageValidator() {
		return WaitUtils.isElemntclickable(addToCartButton);
	}
	
	@Step("Add item to cart.")
	private void addToCart() {
		Assert.assertTrue(WaitUtils.isElemntclickable(addToCartButton),
				"Failed as add to cart element is not clickable");
		getSelectedItemDetails();
		addToCartButton.click();
		logger.info("Successfully clicked on add to cart button");
	}

	@Step("Click on continue to shopping.")
	private void clickContinueShopping() {
		Assert.assertTrue(WaitUtils.isElemntclickable(continueShoppingButton),
				"Failed as continue to shopping button is not clickable.");
		getAddedItemDetails();
		continueShoppingButton.click();
		logger.info("Successfully clicked on continue shopping button");
		Assert.assertTrue(WaitUtils.isElemntInVisble(cartPop), "Failed as Cart pop up is still visiable");
	}
	
	@Step("Verify after adding to cart, Color and size remain the same.")
	public AddToCartPage verifyItemsDetails() {
		addToCart();
		clickContinueShopping();
		Assert.assertEquals(beforeAddColor, AfterAddColor, "Failed as color did not match, after adding to cart");
		Assert.assertEquals(beforeAddSize, AfterAddSize, "Failed as size did not match,after adding to cart");
		return this;
	}

	@Step("Verify any random size of item could be added.")
	public AddToCartPage selectDifferentSize() {
		selectSize();
		addToCart();
		clickContinueShopping();
		Assert.assertEquals(beforeAddColor, AfterAddColor, "Failed as color did not match, after adding to cart");
		Assert.assertEquals(beforeAddSize, AfterAddSize, "Failed as size did not match,after adding to cart");
		return this;
	}

	@Step("Select random Size")
	public void selectSize() {
		Assert.assertTrue(WaitUtils.isElemntVisble(sizeSelect), "Failed to wait for size selection.");
		List<WebElement> options = SelectUtils.getSelect(sizeSelect).getOptions();
		int randomOptionToSelect = RandomDataUtils.randomNumber(1, options.size() - 1);
		SelectUtils.getSelect(sizeSelect).selectByVisibleText(options.get(randomOptionToSelect).getText());
	}

	@Step("Verify cart number is increased after adding.")
	public AddToCartPage verifyCartCounts(int count) {
		addToCart();
		clickContinueShopping();
		Assert.assertTrue(WaitUtils.isElemntVisble(cartCountLink),
				"Failed as cart count link is not visiable to the user");
		int countFromApp = Integer.parseInt(cartCountLink.getText().replaceAll("[^0-9]", "").trim());
		System.out.println("cart count");
		logger.info("The total count from web app is : " + countFromApp);
		Assert.assertEquals(countFromApp, count,
				"Failed as the total count expected was :" + cartCountLink + " but actual is : " + countFromApp);
		return this;
	}
	
	private void getSelectedItemDetails() {
		this.beforeAddColor = SelectUtils.getSelect(colorSelect).getFirstSelectedOption().getText();
		this.beforeAddSize = SelectUtils.getSelect(sizeSelect).getFirstSelectedOption().getText();
		logger.info(
				"Selected Item before adding to cart, Color is : " + beforeAddColor + " and Size is :" + beforeAddSize);
	}

	private void getAddedItemDetails() {
		this.AfterAddColor = getDetails(colorSizeDeatil.get(0), ":").trim();
		this.AfterAddSize = getDetails(colorSizeDeatil.get(1), ":").trim();
		logger.info(
				"Selected Item after adding to cart, Color is : " + AfterAddColor + " and Size is :" + AfterAddSize);
	}

	private String getDetails(WebElement element, String delemeter) {
		String[] temp;
		temp = element.getText().split(delemeter);
		int count = 0;
		for (String singValue : temp) {
			if (count == 1) {
				return singValue;
			}
			count++;
		}
		return null;
	}

	public static class Sizes {
		public static final String EXTRA_SMALL = "XS";
		public static final String SMALL = "S";
		public static final String MEDIUM = "M";
		public static final String LARGE = "L";
		public static final String EXTRA_LARGE = "xl";

	}
	
	 

}
