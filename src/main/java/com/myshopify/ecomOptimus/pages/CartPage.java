package com.myshopify.ecomOptimus.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.myshopify.ecomOptimus.Locators.Locators;
import com.myshopify.ecomOptimus.utils.WaitUtils;

import io.qameta.allure.Step;

public class CartPage extends BasePage {

	@FindBy(className = Locators.CartLoctor.PAGE_VALIDATOR)
	private WebElement pageValidator;

	@FindBy(xpath = Locators.CartLoctor.TABLE_COLUMN)
	private List<WebElement> cartCoulmn;

	@FindBy(xpath = Locators.CartLoctor.TABLE_ROW)
	private List<WebElement> cartRows;

	@FindBy(xpath = Locators.CartLoctor.SUB_TOTAL)
	private WebElement subTotalPrice;

	public CartPage() {
		pageValidator();
	}

	@Override
	public boolean pageValidator() {
		return WaitUtils.isElemntVisble(pageValidator);
	}

	@Step("Verify the total of selected item price is same as of sub total price.")
	public void verifyPrice() {
		Assert.assertTrue(WaitUtils.isElemntVisble(cartRows), "Failed as coloumn of cart is not visiable.");

		double expectedSubTotal = 0;
		for (WebElement row : cartRows) {
			List<WebElement> coulmn = row.findElements(By.tagName("td"));

			WebElement price = coulmn.get(1);
			double actualPrice = Double
					.parseDouble(price.getText().replaceFirst("[.]", "").replaceAll("[^0-9.]", "").trim());

			WebElement quantity = coulmn.get(2).findElement(By.className("cart__qty-input"));
			int actualQuantity = Integer.parseInt(quantity.getAttribute("value"));

			expectedSubTotal = expectedSubTotal + (actualPrice * actualQuantity);
		}
		double actualSubTotal = Double
				.parseDouble(subTotalPrice.getText().replaceFirst("[.]", "").replaceAll("[^0-9.]", "").trim());
		Assert.assertEquals(Math.round(actualSubTotal), Math.round(actualSubTotal),
				"Failed as actual total is : " + Math.round(actualSubTotal) + ", which is not same as expected " + Math.round(actualSubTotal));

	}

}
