package com.myshopify.ecomOptimus.pages;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.myshopify.ecomOptimus.Locators.Locators;
import com.myshopify.ecomOptimus.utils.BaseWebdriver;
import com.myshopify.ecomOptimus.utils.WaitUtils;

import io.qameta.allure.Step;

import com.myshopify.ecomOptimus.utils.LoggerClass;

public class OnBoardingPage {

	@FindBy(id = Locators.OnBoardingLocators.INPUT_EMAIL)
	private WebElement emailValidation;

	@FindBy(xpath = Locators.OnBoardingLocators.LINK_PASSWORD_LOGIN)
	private WebElement loginLink;

	@FindBy(id = Locators.OnBoardingLocators.INPUT_PASSWROD)
	private WebElement passwordLogin;

	@FindBy(xpath = Locators.OnBoardingLocators.BUTTON_SUBMIT)
	private WebElement submitButton;

	@FindBy(id = Locators.OnBoardingLocators.SPAN_INVALID_PASSWORD)
	private WebElement invalidPassword;

	private final String password = "idgad";

	private static final Logger logger = LoggerClass.createLogger();

	public OnBoardingPage() {
		PageFactory.initElements(BaseWebdriver.getDriver(), this);
		Assert.assertTrue(pageValidator(), "Failed to verify Onbarding page as to type email box in not present.");
	}

	private boolean pageValidator() {
		return WaitUtils.isElemntVisble(emailValidation);
	}

	private void clickLoginButton() {
		Assert.assertTrue(WaitUtils.isElemntclickable(loginLink), "Failed as login via password link is not clickable");
		loginLink.click();
		logger.info("Successfully clicked on login via password link.");
	}

	private void enterPassword(String password) {
		Assert.assertTrue(WaitUtils.isElemntVisble(passwordLogin), "Failed as input box for password is not visiable.");
		passwordLogin.clear();
		passwordLogin.sendKeys(password);
		logger.info("Successfully enter password as: " + password + " in password input box");
	}

	private void clickSubmit() {
		Assert.assertTrue(WaitUtils.isElemntclickable(submitButton), "Failed as submit button is not clickable.");
		submitButton.click();
		logger.info("Successfully clicked on Submit button.");
	}

	@Step("Login to ecom-optimus.")
	public HomePage passwordLogin() {
		clickLoginButton();
		enterPassword(this.password);
		clickSubmit();
		return new HomePage();

	}

	public HomePage passwordLogin(String password) {
		clickLoginButton();
		enterPassword(password);
		clickSubmit();
		return new HomePage();

	}

	@Step("verify invalid user not able to log in.")
	public boolean invalidLoginValidation(String password) {
		clickLoginButton();
		enterPassword(password);
		clickSubmit();

		return invalidPassword.isDisplayed();
	}

}
