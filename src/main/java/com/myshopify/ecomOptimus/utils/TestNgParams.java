package com.myshopify.ecomOptimus.utils;

import java.util.Map;

public class TestNgParams {

	private static Map<String, String> allParameters;

	public static void setSuiteParametrs(Map<String, String> suiteParams) {
		TestNgParams.allParameters = suiteParams;
	}

	public static Map<String, String> getAllTestNgParams() {
		try {
			return allParameters;
		} catch (NullPointerException e) {
			// logger for error as its not assigned
			return null;
		}

	}

	public static String getTestNgParam(String akey) {
		return allParameters.get(akey);

		// try catch and error message for exceptions
	}

}
