package com.myshopify.ecomOptimus.utils;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SelectUtils {

	private static final Logger logger = LoggerClass.createLogger();

	public static Select getSelect(WebElement element) {
		return new Select(element);
	}

	public static boolean selectBasedOnVisibleText(WebElement webElement, String visibleText) {
		try {
			assertTrue(WaitUtils.isElemntVisble(webElement), "Failed to view select option");
			Select select = new Select(webElement);
			select.selectByVisibleText(visibleText);
			return true;

		} catch (ElementNotSelectableException e) {
			logger.error("Failed to select due to ", e);
			return false;
		}
	}

}
