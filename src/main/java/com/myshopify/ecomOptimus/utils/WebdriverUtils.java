package com.myshopify.ecomOptimus.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class WebdriverUtils {

	private static final Logger logger = LoggerClass.createLogger();
	 

	public static void scrolIntoElement(WebElement aElement) {
		((JavascriptExecutor) BaseWebdriver.getDriver()).executeScript("arguments[0].scrollIntoView(true);", aElement);
	}

	public static Actions getActionMethod() {
		return new Actions(BaseWebdriver.getDriver());
	}

	public static void javaScriptToWaitForPage() {
		String pageLoadStatus;
		do {
			JavascriptExecutor js = (JavascriptExecutor) BaseWebdriver.getDriver();
			pageLoadStatus = (String) js.executeScript("return document.readyState");
		} while (!pageLoadStatus.endsWith("complete"));
	}

	public static boolean switchToFrame(WebElement webElement) {
		try {

			WaitUtils.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(webElement));
			return true;

		} catch (NoSuchFrameException e) {
			logger.error("Failed to switch to frame as  ", e);
			return false;
		}
	}

}
