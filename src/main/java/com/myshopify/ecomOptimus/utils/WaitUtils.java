package com.myshopify.ecomOptimus.utils;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class WaitUtils {

	final static WebDriverWait wait = new WebDriverWait(BaseWebdriver.getDriver(), Configuration.getExplicit());
	private static final Logger logger = LoggerClass.createLogger();

	public static boolean isElemntVisble(WebElement webElement) {
		try {
			wait.until(ExpectedConditions.visibilityOf(webElement));
			return true;
		} catch (ElementNotVisibleException e) {
			logger.error("Could not find visablity of element " + webElement + " " + e.getMessage());
			return false;
		}
	}

	public static boolean isElemntInVisble(List<WebElement> webElement) {
		try {
			wait.until(ExpectedConditions.invisibilityOfAllElements(webElement));
			return true;
		} catch (Exception e) {
			logger.error("Could not wait for  Invisablity of element " + webElement + " " + e.getMessage());
			return false;
		}
	}
	
	public static boolean isElemntInVisble(WebElement webElement) {
		try {
			wait.until(ExpectedConditions.invisibilityOf(webElement));
			return true;
		} catch (Exception e) {
			logger.error("Could not wait for  Invisablity of element " + webElement + " " + e.getMessage());
			return false;
		}
	}

	public static boolean isElemntclickable(WebElement webElement) {
		try {
			Assert.assertTrue(isElemntVisble(webElement));
			wait.until(ExpectedConditions.elementToBeClickable(webElement));
			return true;
		} catch (ElementClickInterceptedException e) {
			logger.error("Could not find element  " + webElement + " as clickable. " + e.getMessage());
			return false;
		}

		catch (Exception e) {
			logger.error("Could not find element  " + webElement + " as clickable. " + e.getMessage());
			return false;
		}
	}

	public static boolean isTextPresent(WebElement webElement, String textToValidate) {
		try {
			Assert.assertTrue(isElemntVisble(webElement), "Failed as element is not visiable ");
			wait.until(ExpectedConditions.textToBePresentInElement(webElement, textToValidate));
			return true;
		} catch (Exception e) {
			logger.error("Could not find Text " + textToValidate + " of element " + ". But found "
					+ webElement.getText() + " " + e.getMessage());
			return false;
		}
	}

	public static boolean isTextToBe(WebElement webElement, String textToValidate) {
		try {
			Assert.assertTrue(isElemntVisble(webElement), "Failed as element is not visiable ");
			wait.until(ExpectedConditions.textToBe((By) webElement, textToValidate));
			return true;
		} catch (Exception e) {
			logger.error("Could not find Text " + textToValidate + " of element " + ". But found "
					+ webElement.getText() + " " + e.getMessage());
			return false;
		}
	}

	public static boolean isElemntVisble(List<WebElement> webElements) {
		try {
			wait.until(ExpectedConditions.visibilityOfAllElements(webElements));
			return true;
		} catch (ElementNotVisibleException e) {
			logger.error("Could not find visablity of all elements " + webElements + " " + e.getMessage());
			return false;
		}
	}

	public static boolean isTitlePresent(String title) {
		try {
			WebdriverUtils.javaScriptToWaitForPage();
			wait.until(ExpectedConditions.titleIs(title));
			return true;
		} catch (Exception e) {
			logger.error("Could not find title as provided : " + title + " " + e.getMessage());
			return false;
		}
	}

}
