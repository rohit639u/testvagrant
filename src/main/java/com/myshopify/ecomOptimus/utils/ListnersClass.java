package com.myshopify.ecomOptimus.utils;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.xml.XmlSuite;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.sun.javafx.PlatformUtil;

import io.qameta.allure.Attachment;

public class ListnersClass implements WebDriverEventListener, ITestListener, IReporter, ISuiteListener {

	public static final Logger logger = LoggerClass.createLogger();
	static String imageLocation = "images/";
	static String reportLocation = "logs/";

	public void afterAlertAccept(WebDriver arg0) {
		logger.debug("Successfully accepted alert...");

	}

	public void afterAlertDismiss(WebDriver arg0) {
		logger.debug("Successfully dismissed alert...");

	}

	public void afterChangeValueOf(WebElement arg0, WebDriver arg1, CharSequence[] arg2) {
		if (arg0.getAttribute("value") != null && !(arg0.getAttribute("value").isEmpty()))
			logger.debug("Successfully sendKeys  \"" + arg0.getAttribute("value") + "\" to web element : "
					+ arg0.toString());
		else
			logger.debug("Successfully cleared value from web element :" + arg0.toString());

	}

	public void afterClickOn(WebElement arg0, WebDriver arg1) {
		logger.debug("Successfully cliecked on element :" + arg0.toString());
		WebdriverUtils.javaScriptToWaitForPage();

	}

	public void afterFindBy(By arg0, WebElement arg1, WebDriver arg2) {
	}

	public void afterNavigateBack(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateForward(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateRefresh(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void afterNavigateTo(String arg0, WebDriver arg1) {
		logger.debug("Navigated to page : " + arg0.trim());

	}

	public void afterScript(String arg0, WebDriver arg1) {
		// TODO Auto-generated method stub

	}

	public void afterSwitchToWindow(String arg0, WebDriver arg1) {
		// TODO Auto-generated method stub

	}

	public void beforeAlertAccept(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeAlertDismiss(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeChangeValueOf(WebElement arg0, WebDriver arg1, CharSequence[] arg2) {
		WebdriverUtils.scrolIntoElement(arg0);
		if (arg0.getText() != null && !(arg0.getText().isEmpty())) {
			logger.debug("Before using sendKeys/clear the value of element is :" + arg0.getAttribute("value"));
		}
		performLocalSlow(arg0);
	}

	public void beforeClickOn(WebElement arg0, WebDriver arg1) {
		WebdriverUtils.javaScriptToWaitForPage();
		WebdriverUtils.scrolIntoElement(arg0);
		logger.debug("trying  to click on element with locaotr as  : " + arg0.toString());
		if (arg0.getText() != null && !(arg0.getText().isEmpty())) {
			logger.debug("About to click on element " + arg0.getText());
		}
		performLocalSlow(arg0);
	}

	private void performLocalSlow(WebElement arg0) {
		highLightElmnt(arg0);
		if (Configuration.isLocalslow()) {
			WebdriverUtils.getActionMethod().pause(java.time.Duration.ofSeconds(Configuration.getTimetoslow())).build()
					.perform();
		}
	}

	public void beforeFindBy(By arg0, WebElement arg1, WebDriver arg2) {
		logger.debug(("Trying to find Element :-> " + arg0.toString()));

	}

	public void beforeNavigateBack(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateForward(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateRefresh(WebDriver arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeNavigateTo(String arg0, WebDriver arg1) {
		logger.debug("About to navigate page :" + arg0.trim());

	}

	public void beforeScript(String arg0, WebDriver arg1) {
		// TODO Auto-generated method stub

	}

	public void beforeSwitchToWindow(String arg0, WebDriver arg1) {

	}

	public void onException(Throwable arg0, WebDriver arg1) {
		String line = new String(new char[100]).replace('\0', '-');
		logger.error("Found exception for test case :-> \"" + Reporter.getCurrentTestResult().getName() + "\"\n" + line
				+ line, arg0);
		logger.info("\n" + line + line + "\n" + line + line + "\n\n");

	}

	public void onTestStart(ITestResult result) {

		String line = new String(new char[130]).replace('\0', '-');
		logger.info("\n");
		logger.info(line);
		logger.info(StringUtils.center("Test case :-->  \"" + result.getName() + "\"  <--: about to start...", 130));
		logger.info(line + "\n\n");
		ExtendedReport.test = ExtendedReport.extent.createTest(result.getName());
	}

	public void onTestSuccess(ITestResult result) {
		ExtendedReport.test.log(Status.PASS, "**passed**");
		String line = new String(new char[120]).replace('\0', '-');
		logger.info(line);
		logger.info(StringUtils.center(String.format("|%30s|", result.getTestClass())
				+ String.format("|%20s|", result.getName()) + String.format("|%10S|", "passed"), 120));
		logger.info(line + "\n");
	}

	public void onTestFailure(ITestResult result) {
		String screenShots = createScreenshot(BaseWebdriver.getDriver());
		Path resourceDirectory;
		resourceDirectory = Paths.get("logs", screenShots);
		takelogs(result.getThrowable().toString());
		try {
			ExtendedReport.test.log(Status.FAIL, result.getThrowable());
			ExtendedReport.test.fail("*ScreenShot*",
					MediaEntityBuilder.createScreenCaptureFromPath(resourceDirectory.toString()).build());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line = new String(new char[120]).replace('\0', '-');
		logger.info(line);
		logger.info(StringUtils.center(String.format("|%30s|", result.getTestClass())
				+ String.format("|%20s|", result.getName()) + String.format("|%10S|", "failed"), 120));
		logger.info(line + "\n");
	}

	public void onTestSkipped(ITestResult result) {

		ExtendedReport.test.log(Status.SKIP, "**Skipped**");
		String line = new String(new char[120]).replace('\0', '-');
		logger.info(line);
		logger.info(StringUtils.center(String.format("|%30s|", result.getTestClass())
				+ String.format("|%20s|", result.getName()) + String.format("|%10S|", "skipped"), 120));
		logger.info(line + "\n");

	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}

	public void onFinish(ITestContext context) {
		ExtendedReport.extent.flush();
		String line = new String(new char[120]).replace('\0', '-');
		int pass = context.getPassedTests().size();
		int fail = context.getFailedTests().size();
		int skip = context.getSkippedTests().size();
		int total = pass + fail + skip;
		logger.info(line);
		logger.info(StringUtils.center("|Total Number of Test cases Executed for suite \""
				+ context.getCurrentXmlTest().getName() + "\" ||" + " " + total + " ||", 120));
		logger.info(line);
		logger.info(StringUtils.center(String.format("|%5S|", "Total Passed") + String.format("|%5s|", pass)
				+ String.format("|%5S|", "Total Failed") + String.format("|%5s|", fail)
				+ String.format("|%5S|", "Total skipped") + String.format("|%5s|", skip), 120));
		logger.info(line + "\n");

	}

	public static String createScreenshot(WebDriver driver) {

		UUID uuid = UUID.randomUUID();
		TakesScreenshot ts = ((TakesScreenshot) driver);
		takeScreenshot(ts);
		File scrFile = ts.getScreenshotAs(OutputType.FILE);
		try {
			// copy file object to designated location
			FileUtils.copyFile(scrFile, new File(reportLocation + imageLocation + uuid + ".png"));
		} catch (IOException e) {
			logger.error("Error while generating screenshot:\n" + e.toString());
		}
		logger.warn("The Failed test case screenshot saved as : " + imageLocation + uuid
				+ ".png. Please find it in logs/images folder ");
		return imageLocation + uuid + ".png";
	}
	
	@Attachment(value = "Failure Screenshot", type = "image/png")
	public static byte[] takeScreenshot(TakesScreenshot ts) {
			// Take a screenshot as byte array and return
			return ts.getScreenshotAs(OutputType.BYTES);
		}
	
	@Attachment(value = "Failure Log", type = "text/plain")
	public static String takelogs(String exception) {
			
			return exception;
		}

	@Override
	public <X> void afterGetScreenshotAs(OutputType<X> arg0, X arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public <X> void beforeGetScreenshotAs(OutputType<X> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterGetText(WebElement arg0, WebDriver arg1, String arg2) {
		logger.debug("Successfully got text as :-> \"" + arg2 + "\" from web element " + arg0.toString());

	}

	@Override
	public void beforeGetText(WebElement arg0, WebDriver arg1) {
		logger.debug("Trying to get text from web element " + arg0.toString());
		highLightElmnt(arg0);

	}

	/// ------Report creation --------

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {

		try {
			// need to run only if no configration failure needs to work on this
			if ((PlatformUtil.isMac() || PlatformUtil.isWindows()) && Configuration.isLocalslow())
				openGenratedReports();
			RuntimeUtils.copyGenratedProperties();
			if (Configuration.isLocalslow())
				logger.warn(
						"local slow is enabled/true, please make sure not to commit & push it with true as in build environment it will make scripts run slow, Very Slow.");
		} catch (Exception ex) {

			ex.printStackTrace();
		}
	}

	private static void openGenratedReports() {
		final File file = new File(Configuration.getAppName() + ".html");
		try {
			Desktop.getDesktop().browse(file.toURI());
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void highLightElmnt(WebElement arg0) {
		// this is for local run should remove If one does not want to use it.
		for (int i = 0; i < 5; i++) {

			JavascriptExecutor js = (JavascriptExecutor) BaseWebdriver.getDriver();
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", arg0,
					"color: yellow; border: 3px solid red;");
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", arg0,
					"color: pink; border: 4px solid pink;");
			try {
				Thread.sleep(7);
			} catch (Exception e) {
				// TODO: handle exception
			}
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", arg0, "");
		}
	}

	@Override
	public void onStart(ISuite suite) {
		String line = new String(new char[50]).replace('\0', '-');
		if (!suite.getXmlSuite().getParameters().isEmpty() && suite.getXmlSuite().getParameters() != null) {
			logger.info(line + "-> Environment Properties overridden from TestNG Suit or via maven command  <-" + line);
			logger.info(suite.getXmlSuite().getAllParameters().toString());
			logger.info(line + "-> Environment Properties overridden from TestNG Suit or via maven command  <-" + line);
		}
		TestNgParams.setSuiteParametrs(suite.getXmlSuite().getAllParameters());
	}

	@Override
	public void onFinish(ISuite suite) {

	}

}
