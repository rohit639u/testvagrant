package com.myshopify.ecomOptimus.utils;

import java.io.File;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtendedReport {

	private static final ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(
			new File(Configuration.getAppName() + ".html"));
	public static final ExtentReports extent = new ExtentReports();
	public static ExtentTest test;

	static {
		ClassLoader classLoader = ExtendedReport.class.getClassLoader();
		File tempFile = new File(classLoader.getResource("testVagrant.jpeg").getFile());
		String js = "$('.brand-logo').text('').append('<img src=" + tempFile.getAbsolutePath()
				+ " width=\"60px;\" height=\"48px;\""
				+ " style=\" margin-bottom: -10px; margin-left: -12px; filter: invert(100%); \" alt=\"Home\">')";
		htmlReporter.loadXMLConfig(new File(System.getProperty("user.dir") + "/report-config.xml"));
		htmlReporter.config().setDocumentTitle(Configuration.getAppName() + "-Automation Report");
		htmlReporter.config().setReportName(Configuration.getAppName() + " -- Test Cases Execution Result");
		htmlReporter.config().setJS(js);
		htmlReporter.setAppendExisting(false);
		extent.attachReporter(htmlReporter);

	}

}
