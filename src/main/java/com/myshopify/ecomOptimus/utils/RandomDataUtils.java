package com.myshopify.ecomOptimus.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.github.javafaker.Faker;

public class RandomDataUtils {

	public static String randompassword() {
		Faker faker = new Faker();
		return faker.internet().password();
	}

	public static int randomNumber() {
		Faker faker = new Faker();
		return faker.number().randomDigit();
	}
	
	public static int randomNumber(int minLength, int maxLength) {
		Faker faker = new Faker();
		return faker.number().numberBetween(minLength, maxLength);
	}
	
	public static String randompassword(int minLength, int maxLength) {
		Faker faker = new Faker();
		return faker.internet().password(minLength, maxLength);
	}

	public static String randomString(int size) {
		Faker faker = new Faker();
		return faker.regexify("[A-Za-z0-9]{" + size + '}');
	}

	public static String getToDayDate(String format) {
		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		LocalDateTime now = LocalDateTime.now();
		String todayDate = dtf.format(now);
		return todayDate;
	}

}
