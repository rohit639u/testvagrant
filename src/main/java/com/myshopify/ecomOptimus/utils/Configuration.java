package com.myshopify.ecomOptimus.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {

	private final static Properties prop = new Properties();
	private static final String configFile = "config.properties";
	static {
		try {
			prop.load(new FileInputStream(configFile));
		} catch (IOException e) {
			LoggerClass.createLogger().error(e.toString());
		}
	}

	private final static String url = setEnvrVariable("url");
	private final static String browser = setEnvrVariable("browser");
	private final static String appName = getProperties("AppName");
	private final static int implicit = Integer.parseInt(getProperties("implicit"));
	private final static int pageLoad = Integer.parseInt(getProperties("pageLoad"));
	private final static int explicit = Integer.parseInt(getProperties("explicit"));
	private final static int script = Integer.parseInt(getProperties("script"));
	private final static boolean localSlow = Boolean.parseBoolean(getProperties("localSlow"));
	private final static int timeToSlow = Integer.parseInt(getProperties("timeToSlow"));
	private final static String userName = getProperties("userName");
	private final static String userPassword = getProperties("userPassword");

	private static String setEnvrVariable(String parameter) {
		// try catch not required keeping for safe purpose.
		try {
			String testNGProp = TestNgParams.getTestNgParam(parameter);
			String mvnProp = System.getProperty(parameter);

			if (mvnProp != null && !mvnProp.isEmpty()) {
				return mvnProp;
			} else if (testNGProp != null && !testNGProp.isEmpty()) {
				return testNGProp;
			} else {
				return getProperties(parameter);
			}
		} catch (NullPointerException e) {
			LoggerClass.createLogger().error(
					"Failed to set environment properties for \"" + parameter + "\" due to:-\n" + e.getMessage());
			return getProperties(parameter);
		} catch (Exception e) {
			LoggerClass.createLogger().error(
					"Failed to set environment properties for \"" + parameter + "\" due to:-\n" + e.getMessage());
			return getProperties(parameter);
		}
	}

	public static String getUsername() {
		return userName;
	}

	public static String getUserpassword() {
		return userPassword;
	}

	public static int getTimetoslow() {
		return timeToSlow;
	}

	public static String getbrowser() {
		return browser;
	}

	public static int getScript() {
		return script;
	}

	private final static String getProperties(String akey) {
		return prop.getProperty(akey);
	}

	public static String getUrl() {
		return url;
	}

	public static int getImplicit() {
		return implicit;
	}

	public static int getPageload() {
		return pageLoad;
	}

	public static int getExplicit() {
		return explicit;
	}

	public static String getAppName() {
		return appName;
	}

	public static boolean isLocalslow() {
		return localSlow;
	}

}
