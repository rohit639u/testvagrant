package com.myshopify.ecomOptimus.utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import com.sun.javafx.PlatformUtil;

@Listeners(com.myshopify.ecomOptimus.utils.ListnersClass.class)
public class BaseWebdriver {

	public static WebDriver driver;
	private static final Logger logger = LoggerClass.createLogger();

	@BeforeSuite(alwaysRun = true)
	public void initializeDriver() {
		if (Configuration.isLocalslow())
			logger.warn(
					"local slow is enabled/true, please make sure not to commit & push it with true as in build environment it will make scripts run slow, Very Slow.");
		setDriverPath();
	}

	@BeforeMethod(alwaysRun = true)
	public void initTest() {
		if (driver != null) {
			driver = null;
		}
		if (Configuration.getbrowser().equalsIgnoreCase("chrome"))
			driver = new ChromeDriver();
		else if (Configuration.getbrowser().equalsIgnoreCase("firefox"))
			driver = new FirefoxDriver();
		else if (Configuration.getbrowser().equalsIgnoreCase("mobileView"))
			driver = mobileView();

		BaseWebdriver.driver = registerEvents(driver);
		settingBrowser();

	}

	private WebDriver registerEvents(WebDriver driver1) {
		EventFiringWebDriver edriver = new EventFiringWebDriver(driver1);
		ListnersClass listner = new ListnersClass();
		edriver.register(listner);
		return edriver;
	}

	@AfterMethod(alwaysRun = true)
	public static void closeBrowser() {
		logger.info("Closing the session.");
		if (driver != null) {
			driver.quit();
		}
	}

	@AfterSuite(alwaysRun = true)
	public static void closeWebDriver() {
		// needs to have workaround for safely close firefox
		if (driver != null & !Configuration.getbrowser().equalsIgnoreCase("firefox"))
			driver.quit();
	}

	private static void settingBrowser() {
		try {
			driver.get(Configuration.getUrl());
			WebdriverUtils.javaScriptToWaitForPage();
			getDriver().manage().timeouts().implicitlyWait(Configuration.getImplicit(), TimeUnit.SECONDS);
			getDriver().manage().timeouts().pageLoadTimeout(Configuration.getPageload(), TimeUnit.SECONDS);
			getDriver().manage().timeouts().setScriptTimeout(Configuration.getScript(), TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.error("Failed to set browser due to:- " + e.getMessage());
		}
	}

	private void setDriverPath() {
		if (Configuration.getbrowser().equalsIgnoreCase("chrome")||Configuration.getbrowser().equalsIgnoreCase("mobileView")) {
			System.setProperty("webdriver.chrome.driver", getPlatform() + "/chromedriver");
		}

		else if (Configuration.getbrowser().equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", getPlatform() + "/geckodriver");
		}

		else {
			logger.error(
					"Please check browser name provided. it should be eitehr firefox or chrome. Framework currently support only firefox and chrome ");
			System.exit(1);
		}

	}

	private Path getPlatform() {
		Path resourceDirectory;
		if (PlatformUtil.isMac()) {
			resourceDirectory = Paths.get("src", "main", "resources", "mac");
			return resourceDirectory.toAbsolutePath();
		} else if (PlatformUtil.isWindows()) {
			resourceDirectory = Paths.get("src", "main", "resources", "windows");
			return resourceDirectory.toAbsolutePath();
		} else if (PlatformUtil.isLinux()) {
			resourceDirectory = Paths.get("src", "main", "resources", "linux");
			return resourceDirectory.toAbsolutePath();
		}

		else
			logger.error("Platform should be one of these Mac/Windows/linux. Failed as test platform is :"
					+ System.getProperty("os.name"));
		return null;
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public WebDriver mobileView() {
		Map<String, Object> deviceMetrics = new HashMap<>();
		deviceMetrics.put("width", 360);
		deviceMetrics.put("height", 640);
		deviceMetrics.put("pixelRatio", 3.0);

		Map<String, Object> mobileEmulation = new HashMap<>();
		mobileEmulation.put("deviceMetrics", deviceMetrics);
		mobileEmulation.put("userAgent",
				"Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
		return new ChromeDriver(chromeOptions);
	}

}
