package com.myshopify.ecomOptimus.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.myshopify.ecomOptimus.pages.AddToCartPage.Sizes;
import com.myshopify.ecomOptimus.pages.OnBoardingPage;
import com.myshopify.ecomOptimus.utils.BaseWebdriver;
import com.myshopify.ecomOptimus.utils.RandomDataUtils;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Stories;
import io.qameta.allure.Story;

@Epic("testVagrant Coding round")
@Feature("QA-SMOKE")

public class TestVagarant extends BaseWebdriver {

	private final String searchItem = "shirt";
	private final int searchSize = 4;
	private final int totalItemCount = 1;
	private final String description = "<p> This I use to provide a link to the test case in test management tool, "
			+ "where steps are written in much cleaner with <b>fail/pass</b> <i>status</i>.<br>"
			+ "For example, <a href=\"https://www.google.com/\"" + "target=\"_blank\">Click Me</a></p>";

	/*
	 * In This Case I am :- 1. adding product from featured Collection. 2.
	 * Increasing the item with different size and checking the price
	 */
	@Severity(SeverityLevel.BLOCKER)
	@Description(description)
	@Story("Price Validation")
	@Test(enabled = true, priority = 0, groups = {
			"smoke" }, description = "verify diffrent sizes and sub total price.")
	private void addToCartMultipleSize() {
		OnBoardingPage preLogin = new OnBoardingPage();
		preLogin.passwordLogin().selectFeaturedCollection().verifyItemsDetails().selectDifferentSize()
				.navigateToCartPage().verifyPrice();

	}

	/*
	 * In This Case I am :- 1. adding product from featured Collection. 2.
	 * Increasing the item and checking the price
	 */
	@Severity(SeverityLevel.BLOCKER)
	@Description(description)
	@Story("Price Validation")
	@Test(enabled = true, priority = 1, groups = { "smoke" }, description = "verify same sizes and sub total price.")
	private void verifyFeaturedPrice() {
		OnBoardingPage preLogin = new OnBoardingPage();
		preLogin.passwordLogin().selectFeaturedCollection().verifyItemsDetails().verifyItemsDetails()
				.navigateToCartPage().verifyPrice();
	}

	/*
	 * In This Case I am :- 1. searching first, 2. verifying size of searched item
	 * to be 4.
	 */
	@Severity(SeverityLevel.CRITICAL)
	@Description(description)
	@Story("Search Validation")
	@Test(enabled = true, priority = 1, groups = { "smoke" }, description = "verify search total must be 4. ")
	private void verifySerchSize() {
		OnBoardingPage preLogin = new OnBoardingPage();
		Assert.assertTrue(preLogin.passwordLogin().searchSize(searchItem, searchSize),
				"Failed as after searching : " + searchItem + "the size is not same as : " + searchSize);
	}

	/*
	 * In This Case I am :- 1. searching first 2. Adding Random Product 3. Verifying
	 * Items details {Color, Size}
	 */
	@Severity(SeverityLevel.BLOCKER)
	@Description(description)
	@Story("Search Validation")
	@Test(enabled = true, priority = 0, groups = {
			"smoke" }, description = "verify the color and size is same after adding to cart.")
	private void verifyItemDetails() {
		OnBoardingPage preLogin = new OnBoardingPage();
		preLogin.passwordLogin().clickonSearchItem(searchItem).verifyItemsDetails();
	}

	/*
	 * In This Case I am verifying after addition of item, count is changing or not.
	 */
	@Severity(SeverityLevel.CRITICAL)
	@Description(description)
	@Story("Cart Validation")
	@Test(enabled = true, priority = 3, groups = {
			"smoke" }, description = "verify the item count should be same in cart as item added")
	private void verifyCartCount() {
		OnBoardingPage preLogin = new OnBoardingPage();
		preLogin.passwordLogin().clickonSearchItem(searchItem).verifyCartCounts(totalItemCount);
	}

	@Severity(SeverityLevel.MINOR)
	@Description(description)
	@Story("Login Validation")
	@Test(enabled = true, priority = 6, groups = { "smoke" }, description = "verify unauthorized access not possiable")
	private void invalidLoginValidation() {
		OnBoardingPage preLogin = new OnBoardingPage();
		Assert.assertTrue(preLogin.invalidLoginValidation(RandomDataUtils.randompassword()),
				"Failed to verify invalid password scenario.");
	}

}
