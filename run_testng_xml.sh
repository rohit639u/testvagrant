#!/bin/bash -l

SUITE=$1

if [ -z "$SUITE" ]; then
  echo "Failed, as no Suite name provided."
  exit -1
fi
# --- Helps in Pipeline model, not good to have it in loacl as it factory set to original version and all changes will be lost  ----
# 				echo "*** resetting clone level. ***"
#                 if ! git clean -fdx ; then
#                 	echo "error: due to some issue, resetting project to initial clone level, \"failed\"."
#                 	exit 1
#                 fi
#---------######-------------------------------
                echo "Successfully reset project to initial clone level."
                branch=`git branch | grep \\* | cut -d ' ' -f2`
                echo "*** Current Working branch is : \"$branch\" ***"
                if [ "$branch" != "master" ]; then
                	echo "*** Changing Working branch to : \"master\" ***"
                    	if ! git checkout master ; then
                        	echo "error: Could not change to master branch. Aborting after reseting branch."
                        	if ! sudo git reset --hard ; then
                           		echo "error: due to some issue, resetting branch \"failed\"."
                            	exit 1
                        	fi
                        echo "Successfully reset branch."
                        exit 1
                    	fi
                echo "*** Working branch changed to:  \"`git branch | grep \\* | cut -d ' ' -f2`\" ***"
                fi
# --- Helps in Pipeline model ----               
              # if ! git pull git@github.com:Organization/projectName.git master --no-ff --no-edit ; then
              # 	echo "error: due to some issue merge with latest master branch \"FAILED\"."
              #      	git reset --hard 
              #         echo "reset \"master\" branch to before merge. \"COMPLETED\"."
              #         exit 1
              # fi
#---------######-------------------------------              

                echo "*** GIT Process \"COMPLETED SUCCESSFULLY\" ***"
                
                echo "*** Initialising automation test cases for ${SUITE}... ***"
                
                mvn clean install -U -DsuiteFile=suites/$SUITE 
                BUILD_STATUS=$?
                if  [ $BUILD_STATUS -ne 0 ]; then
                       echo "*** Failed with status code as :- $BUILD_STATUS ***"
                       exit 1 
                fi
                echo "*** Test cases COMPLETED. ***"
    
    			echo "*** Initialising Allure Report. Press <Ctrl+C> to exit ... ***"
    			
    			allure serve
    			
               echo "*** DONE.Thanks for using for demo. ***"